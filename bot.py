from dotenv import load_dotenv
import discord
import os
from discord.ext import commands, tasks
import requests
import json
from PIL import ImageGrab
from PIL import Image
from pytesseract import image_to_string
import pytesseract
import os
import mysql.connector
import time

import Datenbank



load_dotenv()
token = os.getenv('DISCORD_TOKEN')
bot = commands.Bot(command_prefix="!")

@bot.event
async def on_ready():
    print(f'{bot.user.name} has connected to Discord!')
    print("Changing Status .......")
    await bot.change_presence(status=discord.Status.idle, activity=discord.Game('Tradechat'))
    print("Status changed")
    tradechat_listener.start()
    print("Bot is ready")
def erkenne_text():
        # Hier muss der Installationspfad von tesseract angegeben werden
        pytesseract.pytesseract.tesseract_cmd = r'D:\tesseract\tesseract'

        print("Text auf dem Bild lautet ...")

        print("... mit dem englischen Wörterbuch:")

        # Normale Textausgabe (sucht nach englischen Woertern) aus dem Bild test.png
        Ergebnis = pytesseract.image_to_string(Image.open('D:\\Screenshots\\ScreenShot_0.png'))

        print("... mit dem deutschen Wörterbuch:")

        # Sucht explizit nach deutschen Woertern, beispielsweise wichtig bei Text mit Umlauten
        #Ergebnis = pytesseract.image_to_string(Image.open('Z:\\Screenshots\\Warframetest_1.png'), lang='deu')
        print(Ergebnis)
        return(Ergebnis)

def Kopie_erstellen():
    os.system('copy D:\\Screenshots\\Chat.txt D:\\Screenshots\\Chats_Copy.txt')
    file = open('D:\\Screenshots\\Chat.txt','w')
    file.close()   
def Kopie_erstellen2():
    os.system('copy D:\\Screenshots\\Chat5.txt D:\\Screenshots\\Chats_Copy2.txt')
    file = open('D:\\Screenshots\\Chat5.txt','w')
    file.close()
def datenbank_eintrag(sql):
    try:
        mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="bSIxubcT1RprBlNi",
        database="api"
        )
        mycursor = mydb.cursor()
        mycursor.execute(sql)
        mydb.commit()
        mycursor.close()
        print("Chatupdated")
    except:
        print("Konnte Chat nicht updaten")

def parser():
    Kopie_erstellen()
    fobj_in = open("D:\\Screenshots\\Chats_Copy.txt")
    i = 1
    old_line = ''
    for line in fobj_in:
        if line != ' ':
            if ':' in line:
                old_line = line
            else:
                old_line = old_line+' '+line
                i = i + 1
                #print(str(i)+': '+old_line)
                old_line=old_line.replace("'","")
                old_line.replace("(","")
                old_line.replace(")","")
                old_line.replace("'","")
                data=old_line.split(':',1)
                #for temp in data:
                    #print(temp)
                if len(data) == 2:
                    sql = "Insert into tradechat2(username,nachricht) values('"+data[0]+"','"+data[1]+"')"
                    sql2 = "Insert into tradechat(user,nachricht) values('"+data[0]+"','"+data[1]+"')"
                    datenbank_eintrag(sql)
                    print("tradechat2")
                    datenbank_eintrag(sql2)
                    print("tradechat")
    fobj_in.close()

def in_datei_schreiben(Ergebnis):
    fobj_in = open("D:\\Screenshots\\Chat.txt", encoding='utf-8')
    fobj_out = open("D:\\Screenshots\\Chat2.txt","w")
    i = 1
    for line in fobj_in:
        print(line.rstrip())
        fobj_out.write(line)
        i = i + 1
    print(str(Ergebnis))
    fobj_out.write(str(Ergebnis))
    fobj_in.close()
    fobj_out.close()
def in_datei_schreiben2(Ergebnis):
    fobj_in = open("D:\\Screenshots\\Chat5.txt")
    fobj_out = open("D:\\Screenshots\\Chat6.txt","w")
    i = 1
    for line in fobj_in:
        print(line.rstrip())
        fobj_out.write(line)
        i = i + 1
    print(str(Ergebnis))
    fobj_out.write(str(Ergebnis))
    fobj_in.close()
    fobj_out.close()
def datei_umbenennen():
    os.remove('D:\\Screenshots\\Chat.txt')
    os.rename('D:\\Screenshots\\Chat2.txt', 'D:\\Screenshots\\Chat.txt')
    file = open('D:\\Screenshots\\Chat2.txt','w')
    file.close()
def datei_umbenennen2():
    os.remove('D:\\Screenshots\\Chat5.txt')
    os.rename('D:\\Screenshots\\Chat6.txt', 'D:\\Screenshots\\Chat5.txt')
    file = open('D:\\Screenshots\\Chat6.txt','w')
    file.close()
def mache_screenshot():
    
        #ImageGrab.grab().save('Z:\\Screenshots\\Warframelauncher.png')
        ImageGrab.grab().save('D:\\Screenshots\\Warframelauncher.png')
        im = Image.open('D:\\Screenshots\\Warframelauncher.png').convert('L')
        im = im.crop((480,400,1490, 830))
        im.save('D:\\Screenshots\\ScreenShot_0.png')
        #imThumbnail = im.resize((5900, 2500), Image.LANCZOS)
        #imThumbnail.save('Z:\\Screenshots\\Warframetest_1.jpg')
        Ergebnis= erkenne_text()
        #Ergebnis = Ergebnis.replace('ı','i')
        #Ergebnis = Ergebnis.encode("utf-8")
        Ergebnis = Ergebnis.encode("utf-8","ignore")
        Ergebnis = Ergebnis.decode('utf-8','strict')
        in_datei_schreiben(Ergebnis)
        in_datei_schreiben2(Ergebnis)
        datei_umbenennen()
        datei_umbenennen2()
        parser()
@tasks.loop(seconds = 1)
async def tradechat_listener():
    try:
        channel = bot.get_channel(731805363094945862)
        print("ich habe den Channel")
        print("Ich checke jetzt den Chat")
        mache_screenshot()
        mydb=Datenbank.get_db()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM tradechat"
        mycursor.execute(sql)
        result = mycursor.fetchall()
        sende_folgendes = ""
        for x in result:
            id = x[0]
            user =x[1]
            nachricht = x[2]
            print(str(id))
            print(user)
            print(nachricht)
            
            await channel.send(str(user))
            await channel.send("```"+str(nachricht)+"```")
        print("Chat printed")
        sql2 = "Insert into tradechatrbeiter (console) values ('2')"
        mycursor.execute(sql2)
        mydb.commit()
        mycursor.close()
        print("Chatupdated")

    except:
        print('Fehler im Tradechat')

bot.run(token)
